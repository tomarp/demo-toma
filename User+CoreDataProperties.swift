//
//  User+CoreDataProperties.swift
//  
//
//  Created by Toma on 7/11/17.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var password: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var imageURL: String?
    @NSManaged public var lastName: NSObject?

}
