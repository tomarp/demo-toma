//
//  NetworkManager.swift
//  Demo
//
//  Created by Toma on 7/11/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class NetworkManager {
    
    static let sharedInstance = NetworkManager()
    
    private init() { }
    
    func getImage(fromURL imageURL:String, completionHandler: @escaping (_ result: UIImage?, _ error: String?) -> ()) {
        print(imageURL)
        if let url = URL(string: imageURL) {
            let session = URLSession.shared
            let task = session.dataTask(with: url) { (result, response, error) in
                if error == nil, let result = result {
                    if let image = UIImage(data: result) {
                        completionHandler(image, nil)
                    }
                    else {
                        completionHandler(nil, String(describing: error))
                    }
                }
                else {
                    completionHandler(nil, String(describing: error))
                }
            }
            task.resume()
        }
        else {
            completionHandler(nil, "Invalid URL ERROR")
        }
    }
    
    func generateFBImageURL(forFBId id: String) -> String {
        let url = "http://graph.facebook.com/" + id + "/picture?width=200&height=200"
        print(url)
        return url
    }
    
    
    func apiRequest(completionHandler: @escaping (_ result: [Movie]?, _ error: Error?) -> ()) {
        
        let parameters: Parameters = ["api_key" : Constants.API_KEY]
        
        Alamofire.request(Constants.BASE_API_LINK, parameters: parameters).validate().responseJSON { response in
            var movies = [Movie]()
            let result = response.result
            switch result {
            case .success:
                if let value = result.value {
                    let json = JSON(value)
                    if let results = json["results"].array {
                        for movie in results {
                            let id = movie["id"].int
                            let voteCount = movie["vote_count"].int
                            let posterPath = movie["poster_path"].string
                            let overview = movie["overview"].string
                            let title = movie["title"].string
                            let movie = Movie(voteCount: voteCount, posterPath: posterPath, overview: overview, title: title, id: id)
                            movies.append(movie)
                        }
                    }
                    completionHandler(movies, nil)
                }
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
    
    
    func apiRequest(forGenre genreId: Int, completionHandler: @escaping (_ result: [Movie]?, _ error: Error?) -> ()) {
        
        print(String(describing: genreId))
        let parameters: Parameters = ["api_key" : Constants.API_KEY, "with_genres": String(describing: genreId)]
        
        Alamofire.request(Constants.BASE_API_LINK, parameters: parameters).validate().responseJSON { response in
            var movies = [Movie]()
            let result = response.result
            switch result {
            case .success:
                if let value = result.value {
                    let json = JSON(value)
                    if let results = json["results"].array {
                        for movie in results {
                            let id = movie["id"].int
                            let voteCount = movie["vote_count"].int
                            let posterPath = movie["poster_path"].string
                            let overview = movie["overview"].string
                            let title = movie["title"].string
                            
                            let movie = Movie(voteCount: voteCount, posterPath: posterPath, overview: overview, title: title, id: id)
                            movies.append(movie)
                        }
                    }
                    completionHandler(movies, nil)
                }
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
    
    func apiRequest(forMovieId id: Int, completionHandler: @escaping (_ result: DetailedMovie?, _ error: Error?) -> ()) {
        
        let parameters: Parameters = ["api_key": Constants.API_KEY]
        
        Alamofire.request(Constants.BASE_API_MOVIE_LINK + "/" + String(describing: id), parameters: parameters).validate().responseJSON { response in

            let result = response.result
            switch result {
            case .success:
                if let value = result.value {
                    let json = JSON(value)
                    
                    let id = json["id"].int
                    let overview = json["overview"].string
                    let releaseDate = json["release_date"].string
                    let title = json["title"].string
                    let posterPath = json["poster_path"].string
                    
                    let detailedMovie = DetailedMovie(id: id, releaseDate: releaseDate, title: title, overview: overview, posterPath: posterPath)
                    completionHandler(detailedMovie, nil)
                }
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
}
