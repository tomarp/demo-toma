//
//  DBManager.swift
//  Demo
//
//  Created by Toma on 7/5/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DBManager {
    
    static let sharedInstance = DBManager()
    private init() {}
    
    var currentUser: User?
    
    public func add(phoneNumber uid: String) {
        var ok = true
        
        fetchUsers{ (result, error) in
            if let error = error {
                print(error)
                ok = false
            }
            if let result = result {
                for user in result {
                    if user.phoneNumber == uid {
                        print("Already exists!")
                        ok = false
                    }
                }
            }
        }
        
        if ok == false {
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let user = User(context: context)
        user.phoneNumber = uid
        
        do {
            try context.save()
            print("Saved phone number")
        }
        catch {
            print("Error when saving phone number")
        }
    }
    
    public func fetchUsers(handler: (_ result: [User]?, _ error: NSError?) -> ())  {
        do {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let users = try context.fetch(User.fetchRequest()) as! [User]
            handler(users, nil)
        }
        catch let error as NSError {
            handler(nil, error)
        }
    }
    
    public func fetchUser(withPhoneNumber phoneNubmer: String) -> User? {
        var foundUser: User?
        fetchUsers { (result, error) in
            if error != nil {
                print("Error when fetching user", error ?? "Unknown error")
            }
            else {
                if let result = result {
                    for user in result {
                        if user.phoneNumber == phoneNubmer {
                            foundUser = user
                            break
                        }
                    }
                }
            }
        }
        return foundUser
    }
    
    public func add(password:String, forPhoneNumber phoneNumber:String) {
        fetchUsers { (result, error) in
            if error != nil {
                print("Error: ", error ?? "Unknown error")
            }
            else if let result = result {
                for user in result {
                    if user.phoneNumber == phoneNumber {
                        user.password = password
                        do {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let context = appDelegate.persistentContainer.viewContext
                            try context.save()
                            print("Saved password")
                        }
                        catch {
                            print("Error when saving password")
                        }
                        
                    }
                }
            }
        }
    }
    
    public func setInfo(forPhoneNumber phoneNumber:String, firstName: String!, lastName: String!, email: String!, fbId: String?) {
        fetchUsers { (result, error) in
            if error != nil {
                print("Error", error ?? "Unknown error")
            }
            else {
                if let result = result {
                    for user in result {
                        if user.phoneNumber == phoneNumber {
                            user.firstName = firstName
                            user.lastName = lastName
                            user.email = email
                            user.fbId = fbId
                        }
                    }
                }
            }
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                try context.save()
                print("Saved phone number")
            }
            catch {
                print("Error when saving phone number")
            }
            
        }
        //        if let user = fetchUser(withPhoneNumber: phoneNumber) {
        //            user.firstName = firstName
        //            user.lastName = lastName
        //            user.email = email
        //            user.fbId = fbId
        //        }
    }
    
    public func goodCredentials(forPhoneNumber phoneNumber:String, password: String) -> Bool {
        
        var ok = false
        
        fetchUsers { (result, error) in
            if error != nil {
                print("Error: ", error ?? "Unknown error")
            }
            else {
                if let result = result {
                    for user in result {
                        if user.phoneNumber == phoneNumber {
                            if user.password == password {
                                ok = true
                            }
                            break
                        }
                    }
                }
            }
        }
        return ok
    }
    
    public func dummyFill() {
        add(phoneNumber: "0000000000")
        add(password: "12345678", forPhoneNumber: "0000000000")
        add(phoneNumber: "1111111111")
        add(password: "12345678", forPhoneNumber: "1111111111")
        
    }
    
    public func setCurrentUser(phoneNumber:String) {
        UserDefaults.standard.set(phoneNumber, forKey: "currentUser")
    }
    
    public func getCurrentUserPhoneNumber() -> String? {
        let phoneNumber = UserDefaults.standard.string(forKey: "currentUser")
        return phoneNumber
    }
    
}
