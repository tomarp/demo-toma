//
//  MapsViewController.swift
//  Demo
//
//  Created by Toma on 7/25/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit
import MapKit

class MapsViewController: UIViewController, MKMapViewDelegate {

    var location = CLLocationCoordinate2DMake(44.4499351, 26.0947863)

    @IBOutlet weak var mapView: MKMapView!
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    func focusOnPin() {
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: true)
        let pin = MKPointAnnotation()
        pin.coordinate = location
        mapView.delegate = self
        mapView.addAnnotation(pin)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if isViewLoaded {
            focusOnPin()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        focusOnPin()
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "custom_pin")
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "custom_pin")
        }
        annotationView!.image = UIImage(named: "heart_icon")
        return annotationView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
