//
//  Constants.swift
//  Page Menu
//
//  Created by Toma on 7/10/17.
//  Copyright © 2017 Toma Radu-Petrescu. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    static let SCROLL_MENU_FONT = UIFont(name: "ProximaNova-Semibold", size: 13)!
    static let SCROLL_MENU_FONT_COLOR = UIColor(red: 173/255, green: 173/255, blue: 173/255, alpha: 1)
    static let MAIN_TINT_COLOR = UIColor(red: 248/255, green: 82/255, blue: 74/255, alpha: 1)
    
    static let BASE_API_LINK = "https://api.themoviedb.org/3/discover/movie"
    static let BASE_API_MOVIE_LINK = "https://api.themoviedb.org/3/movie"
    static let API_KEY = "1906833e4f7429ebcd27cb7a04da7c0a"
    static let BASE_API_IMAGE_LINK = "https://image.tmdb.org/t/p/w500"
    
}
