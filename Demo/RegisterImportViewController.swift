//
//  RegisterImportViewController.swift
//  Demo
//
//  Created by Toma on 7/6/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class RegisterImportViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var loginButton: FBSDKLoginButton!
    @IBOutlet weak var wrappingView: UIView!
    
    var password = ""
    var phoneNumber = ""
    var firstName: String!
    var lastName: String!
    var email: String!
    var fbId: String?
    
    func roundLeftCorners(for view: UIView) {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: 5, height: 5)).cgPath as CGPath
        view.layer.mask = maskLayer
        view.setNeedsLayout()
    }
    
    private func roundRightCorners(for view: UIView) {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 5, height: 5)).cgPath as CGPath
        view.layer.mask = maskLayer
        view.setNeedsLayout()
    }
    
    override func viewWillLayoutSubviews() {
        wrappingView.layer.cornerRadius = 5.0
        roundLeftCorners(for: firstNameView)
        roundRightCorners(for: lastNameView)
        emailView.layer.cornerRadius = 5.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        
        print(password, phoneNumber)
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func fbImportPressed(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self)
        { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((FBSDKAccessToken.current()) != nil) {
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, email, id"]).start(completionHandler:
                                { (connection, result, error) -> Void in
                                    if (error == nil) {
                                        
                                        if let dict = result as? [String : AnyObject] {
                                            weak var weakSelf = self
                                            
                                            self.firstName = dict["first_name"] as! String
                                            self.lastName = dict["last_name"] as! String
                                            self.email = dict["email"] as! String
                                            self.fbId = dict["id"] as? String
                                            
                                            weakSelf?.firstNameTextField.text = self.firstName
                                            weakSelf?.lastNameTextField.text = self.lastName
                                            weakSelf?.emailTextField.text = self.email
                                            
                                            self.firstNameTextField.isUserInteractionEnabled = false
                                            self.lastNameTextField.isUserInteractionEnabled = false
                                            self.emailTextField.isUserInteractionEnabled = false
                                        }
                                        
                                    }
                                    else {
                                        print("Error:", error ?? "unkown error")
                                        let alert = UIAlertController(title: "Error", message: "We encountered an error while connecting to Facebook", preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                                            alert.dismiss(animated: true, completion: nil)
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func isValid(emailAddress email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func presentAllFieldsAlert() {
        let alert = UIAlertController(title: "Error", message: "All the fields must be filled", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func allFieldsAreValid() -> Bool {
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" {
            presentAllFieldsAlert()
            return false
        }
        
        if let email = emailTextField.text, isValid(emailAddress: email) {
            return true
        }
        
        let alert = UIAlertController(title: "Error", message: "Pleaes enter a valid e-mail address", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
        return false
    }
    
    private func addUser(withPhoneNumber phoneNumber: String, password: String) {
        DBManager.sharedInstance.add(phoneNumber: phoneNumber)
        DBManager.sharedInstance.add(password: password, forPhoneNumber: phoneNumber)
        DBManager.sharedInstance.setInfo(forPhoneNumber: phoneNumber, firstName: firstName, lastName: lastName, email: email, fbId: fbId)
        DBManager.sharedInstance.setCurrentUser(phoneNumber: phoneNumber)
        CurrentUser.sharedInstance.set(user: DBManager.sharedInstance.fetchUser(withPhoneNumber: phoneNumber))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            lastNameTextField.becomeFirstResponder()
            return true
        }
        if textField == lastNameTextField {
            emailTextField.becomeFirstResponder()
            return true
        }
        if textField == emailTextField {
            if allFieldsAreValid() {
                addUser(withPhoneNumber: phoneNumber, password: password)
                self.performSegue(withIdentifier: "main_screen", sender: nil)
                return false
            }
        }
        return true
    }
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        if allFieldsAreValid() {
            addUser(withPhoneNumber: phoneNumber, password: password)
            self.performSegue(withIdentifier: "main_screen", sender: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func handleFirstNameViewTap(_ sender: UITapGestureRecognizer) {
        firstNameTextField.becomeFirstResponder()
    }
    
    @IBAction func handleLastNameViewTap(_ sender: UITapGestureRecognizer) {
        lastNameTextField.becomeFirstResponder()
    }
    
    @IBAction func handleEmailViewTap(_ sender: UITapGestureRecognizer) {
        emailTextField.becomeFirstResponder()
    }
    
}
