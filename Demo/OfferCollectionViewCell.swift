//
//  OfferCollectionViewCell.swift
//  Demo
//
//  Created by Toma on 7/12/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bigLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        view.layer.cornerRadius = 5.0
        
        loveButton.layer.borderColor = loveButton.titleLabel?.textColor.cgColor
        loveButton.layer.borderWidth = 0.3
        loveButton.layer.cornerRadius = 5.0
    }
}
