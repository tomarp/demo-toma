//
//  PagingMenuControllerOptions.swift
//  Page Menu
//
//  Created by Toma Radu-Petrescu on 09/07/2017.
//Users/toma/Desktop/Swift/Page Menu Controller/Page Menu/Page Menu/PagingMenuControllerOptions.swift/  Copyright © 2017 Toma Radu-Petrescu. All rights reserved.
//

import Foundation

struct PagingMenuOptions: PagingMenuControllerCustomizable {
    var allViewController = OffersViewController.instantiateFromStoryboard()
    var couponsViewController = OffersViewController.instantiateFromStoryboard()
    var discountsViewController = OffersViewController.instantiateFromStoryboard()
    var newViewController = OffersViewController.instantiateFromStoryboard()
    var salesViewController = OffersViewController.instantiateFromStoryboard()

    
    var componentType: ComponentType {
        return .all(menuOptions: MenuOptions(), pagingControllers: [allViewController, couponsViewController, discountsViewController, newViewController, salesViewController])
    }

    var lazyLoadingPage: LazyLoadingPage {
        return .all
    }
    
    struct MenuOptions: MenuViewCustomizable {
        
        var height = 45.0
        
        var isAutoSelectAtScrollEnd = false
        
        var displayMode: MenuDisplayMode {
            return .standard(widthMode: MenuItemWidthMode.flexible, centerItem: false, scrollingMode: MenuScrollingMode.scrollEnabled)
        }
        var focusMode: MenuFocusMode {
            return .underline(height: 5, color: Constants.MAIN_TINT_COLOR, horizontalPadding: 0, verticalPadding: 0)
        }
        var itemsOptions: [MenuItemViewCustomizable] {
            return [FirstMenuItem(), SecondMenuItem(), ThirdMenuItem(), FourthMenuItem(), FifthMenuItem()]
        }
    }
    
    init() {
        allViewController.categoryType = CategoryType.all
        couponsViewController.categoryType = CategoryType.coupons
        discountsViewController.categoryType = CategoryType.discounts
        newViewController.categoryType = CategoryType.new
        salesViewController.categoryType = CategoryType.sales
    }
}

struct FirstMenuItem: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
//        let atrText = NSAttributedString(string: "COUPONS", attributes: [NSFontAttributeName: Constants.SCROLL_MENU_FONT,  NSForegroundColorAttributeName: Constants.SCROLL_MENU_FONT_COLOR])
//        let label = UILabel()
//        label.attributedText = atrText
//        label.sizeToFit()
//        var frame = label.frame
//        frame.size.width += 50
//        frame.size.height += 40
//        label.frame = frame
//        label.textAlignment = .center
//        return .custom(view: label)
        
        let text = MenuItemText(text: "ALL", color: Constants.SCROLL_MENU_FONT_COLOR, selectedColor: Constants.MAIN_TINT_COLOR, font: Constants.SCROLL_MENU_FONT, selectedFont: Constants.SCROLL_MENU_FONT)
        return .text(title: text)
    }
}

struct SecondMenuItem: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let text = MenuItemText(text: "ADVENTURE", color: Constants.SCROLL_MENU_FONT_COLOR, selectedColor: Constants.MAIN_TINT_COLOR, font: Constants.SCROLL_MENU_FONT, selectedFont: Constants.SCROLL_MENU_FONT)
        return .text(title: text)
    }
}

struct ThirdMenuItem: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let text = MenuItemText(text: "ANIMATION", color: Constants.SCROLL_MENU_FONT_COLOR, selectedColor: Constants.MAIN_TINT_COLOR, font: Constants.SCROLL_MENU_FONT, selectedFont: Constants.SCROLL_MENU_FONT)
        return .text(title: text)
    }
}

struct FourthMenuItem: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let text = MenuItemText(text: "COMEDY", color: Constants.SCROLL_MENU_FONT_COLOR, selectedColor: Constants.MAIN_TINT_COLOR, font: Constants.SCROLL_MENU_FONT, selectedFont: Constants.SCROLL_MENU_FONT)
        return .text(title: text)
    }
}

struct FifthMenuItem: MenuItemViewCustomizable {
    var displayMode: MenuItemDisplayMode {
        let text = MenuItemText(text: "FAMILY", color: Constants.SCROLL_MENU_FONT_COLOR, selectedColor: Constants.MAIN_TINT_COLOR, font: Constants.SCROLL_MENU_FONT, selectedFont: Constants.SCROLL_MENU_FONT)
        return .text(title: text)
    }
}
