//
//  CurrentUser.swift
//  Demo
//
//  Created by Toma on 7/11/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import Foundation

class CurrentUser {
    
    private var user: User?
    
    func set(user: User?) {
        self.user = user
    }
    
    func getUser() -> User? {
        return user
    }
    
    static let sharedInstance = CurrentUser()
    
    private init() {}
}
