//
//  OffersViewController.swift
//  Page Menu
//
//  Created by Toma Radu-Petrescu on 09/07/2017.
//  Copyright © 2017 Toma Radu-Petrescu. All rights reserved.
//

import UIKit
import Kingfisher
import MapKit

class OffersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var categoryType: CategoryType!
    
    var movies = [Movie]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        self.view.layoutIfNeeded()
        
        apiRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instantiateFromStoryboard() -> OffersViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OffersViewController
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func apiRequest(forGenre genreId: Int) {
        NetworkManager.sharedInstance.apiRequest(forGenre: genreId, completionHandler: { (results, error) in
            if error != nil {
                print("Error", error!)
            }
            else {
                if let results = results {
                    self.movies = results
                    self.collectionView.reloadData()
                }
            }
        })
    }
    
    func apiRequestForAll() {
        NetworkManager.sharedInstance.apiRequest(completionHandler: { (results, error) in
            if error != nil {
                print("Error", error!)
            }
            else {
                if let results = results {
                    self.movies = results
                    self.collectionView.reloadData()
                }
            }
        })
    }
    
    func apiRequest() {
        if let type = categoryType {
            switch type {
            case .all:
                print("ALL")
                apiRequestForAll()
            case .coupons:
                print("COUPONS")
                apiRequest(forGenre: 12)
            case .discounts:
                print("DISCOUNTS")
                apiRequest(forGenre: 16)
            case .new:
                print("NEW")
                apiRequest(forGenre: 35)
            case .sales:
                print("SALES")
                apiRequest(forGenre: 10751)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! OfferCollectionViewCell
        
        let movie = movies[indexPath.row]
        cell.descriptionLabel.text = movie.overview
        cell.bigLabel.text = movie.title
        if let count = movie.voteCount {
            cell.loveButton.setTitle(String(describing: count), for: .normal)
        }
        else {
            cell.loveButton.setTitle("", for: .normal)
        }
        
        
        if let posterPath = movie.posterPath {
            if let url = URL(string: Constants.BASE_API_IMAGE_LINK + posterPath) {
                let resource = ImageResource(downloadURL: url, cacheKey: nil)
                cell.imageView.kf.indicatorType = .activity
                cell.imageView.kf.setImage(with: resource, placeholder: nil, options: nil , progressBlock: nil, completionHandler: nil)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row % 3 == 0 {
            return CGSize(width: collectionView.frame.width, height: 390)
        }
        return CGSize(width: collectionView.frame.width / 2 - 4,  height: 310)
    }
    
    func makeRequest() {
        print(categoryType)
    }
    
    @IBAction func pinButtonPressed(_ sender: UIButton) {
        if let mapVC = tabBarController?.viewControllers?[2] as? MapsViewController {
            mapVC.location = CLLocationCoordinate2DMake(37.3092828, -122.061154)
            tabBarController?.selectedIndex = 2
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "movie_detail":
                if let vc = segue.destination as? OfferDetailViewController {
                    let cell = sender as! OfferCollectionViewCell
                    let indexPath = collectionView.indexPath(for: cell)
                    vc.id = movies[(indexPath?.row)!].id
                }
            default:
                break
            }
        }
    }
    

}
