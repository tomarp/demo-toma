enum CategoryType {
    case all
    case coupons
    case discounts
    case new
    case sales
}
