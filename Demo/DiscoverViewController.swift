//
//  DiscoverViewController.swift
//  Page Menu
//
//  Created by Toma Radu-Petrescu on 09/07/2017.
//  Copyright © 2017 Toma Radu-Petrescu. All rights reserved.
//

import UIKit

class DiscoverViewController: UIViewController, UITextFieldDelegate {
    
    var options = PagingMenuOptions()
    var searchView: SearchUIView?
    @IBOutlet weak var searchButton: UIBarButtonItem!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureDesign() {
        searchButton.image = UIImage(named: "nav_search_icon")?.withRenderingMode(.alwaysOriginal)
        navigationController?.navigationBar.barTintColor = UIColor(red: 248/255, green: 82/255, blue: 74/255, alpha: 1)
        let titleLabel = UILabel()
        titleLabel.attributedText = NSAttributedString(string: "OFFERS", attributes: [NSFontAttributeName: UIFont(name: "ProximaNova-Semibold", size: 17) ?? .systemFont(ofSize: 17), NSForegroundColorAttributeName: UIColor.white, NSKernAttributeName: 2.5])
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
    
    func addSearchView() {
        if myCustomView == nil { // make it only once
            myCustomView = Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)?.first as? SearchUIView
            myCustomView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            self.view.addSubview(myCustomView!)
            searchView = myCustomView
            searchView?.isHidden = true
            self.searchView?.textField.delegate = self
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handle))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
        }
    }
    
    func configurePagingController() {
        let pagingMenuController = self.childViewControllers.first as! PagingMenuController
        pagingMenuController.setup(options)
//        pagingMenuController.onMove = { state in
//            
//            switch state {
//            case let .willMoveController(menuController, previousMenuController):
//                print(previousMenuController)
//                print(menuController)
//            case let .didMoveController(menuController, previousMenuController):
//                print(previousMenuController)
//                print(menuController)
//            case let .willMoveItem(menuItemView, previousMenuItemView):
//                print(previousMenuItemView)
//                print(menuItemView)
//            case let .didMoveItem(menuItemView, previousMenuItemView):
//                print(previousMenuItemView)
//                print(menuItemView)
//            case .didScrollStart:
//                print("Scroll start")
//            case .didScrollEnd:
//                print("Scroll end")
//            }
//        }
    }
    
    func handle() {
        searchView?.textField.resignFirstResponder()
        self.searchView?.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDesign()
        addSearchView()
        configurePagingController()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.searchView?.textField.text = ""
        return true
    }
    
    var myCustomView: SearchUIView? // declare variable inside your controller
    
    @IBAction func searchButtonPressed(_ sender: UIBarButtonItem) {
        searchView?.isHidden = false
    }

    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.searchView?.isHidden = true
        self.searchView?.textField.text = "Search for something"
        searchView?.textField.resignFirstResponder()
    }
}
