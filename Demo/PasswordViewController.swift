//
//  PasswordViewController.swift
//  Demo
//
//  Created by Toma on 7/6/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordConfirmationView: UIView!
    
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordView.layer.cornerRadius = 5.0
        passwordConfirmationView.layer.cornerRadius = 5.0

        hideKeyboardWhenTappedAround()
        
        passwordTextField.delegate = self
        passwordConfirmationTextField.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "registration_import":
                if let vc = segue.destination as? RegisterImportViewController {
                    if let password = passwordConfirmationTextField.text {
                        vc.password = password
                        vc.phoneNumber = phoneNumber
                    }
                }
            default:
                break
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            passwordConfirmationTextField.becomeFirstResponder()
            return true
        }
        if textField == passwordConfirmationTextField {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    public func validate(firstField field1:UITextField!, confirmationField field2:UITextField!) {
        if let password1 = field1.text, let password2 = field2.text {
            if password1.characters.count < 8 {
                let alert = UIAlertController(title: "Error", message: "The password must be at least 8 characters long", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            if password1 == password2 {
                performSegue(withIdentifier: "registration_import", sender: nil)
                return
            }
            else {
                let alert = UIAlertController(title: "Error", message: "The passwords do not match", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        
        let alert = UIAlertController(title: "Error", message: "An unknown error occured", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        return
    }

    @IBAction func nextButtonPressed(_ sender: UIButton) {
        validate(firstField: passwordTextField, confirmationField: passwordConfirmationTextField)
    }
    
    @IBAction func handlePasswordTap(_ sender: UITapGestureRecognizer) {
        passwordTextField.becomeFirstResponder()
    }
    
    @IBAction func handleConfirmationTap(_ sender: UITapGestureRecognizer) {
        passwordConfirmationTextField.becomeFirstResponder()
    }
}
