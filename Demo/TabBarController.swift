//
//  TabBarController.swift
//  Demo
//
//  Created by Toma on 7/10/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    func scaledImage(_ image: UIImage, maximumWidth: CGFloat) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        let cgImage: CGImage = image.cgImage!.cropping(to: rect)!
        return UIImage(cgImage: cgImage, scale: image.size.width / maximumWidth, orientation: image.imageOrientation)
    }
    
    func maskRoundedImage(image: UIImage, radius: Float) -> UIImage {
        let imageView: UIImageView = UIImageView(image: image)
        var layer: CALayer = CALayer()
        layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = CGFloat(radius)
        UIGraphicsBeginImageContext(imageView.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        removeTabbBarItemsText()
        tabBar.tintColor = Constants.MAIN_TINT_COLOR
        tabBar.barTintColor = UIColor(red: 34/255, green: 40/255, blue: 43/255, alpha: 1)
        tabBar.isTranslucent = false
        
        tabBar.selectionIndicatorImage = underlineTabBarItem(color: Constants.MAIN_TINT_COLOR, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height), lineWidth: 10.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if let fbId = CurrentUser.sharedInstance.getUser()?.fbId {
            let imageURL = NetworkManager.sharedInstance.generateFBImageURL(forFBId: fbId)
            print(imageURL)
            NetworkManager.sharedInstance.getImage(fromURL: imageURL) { (image, error) in
                if error != nil {
                    print("Error", error ?? "Unknown error")
                }
                else {
                    if let image = image {
                        let tabItems = self.tabBar.items
                        let tabItem = tabItems?[3]
                        DispatchQueue.main.async {
                            let roundedImage = self.maskRoundedImage(image: image, radius: Float(image.size.height/CGFloat(2)))
                            let resizedImage = self.scaledImage(roundedImage, maximumWidth: 30)
                            tabItem?.image = resizedImage.withRenderingMode(.alwaysOriginal)
                        }
                    }
                }
            }
        }
        else {
            if let image = UIImage(named: "default_user_icon") {
                let tabItems = self.tabBar.items
                let tabItem = tabItems?[3]
                tabItem?.image = image.withRenderingMode(.alwaysOriginal)
            }
        }
    }
    
    func underlineTabBarItem(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let path = UIBezierPath(roundedRect: CGRect(x: 7, y: size.height - lineWidth + 5, width: size.width - 14, height: lineWidth), cornerRadius: 5.0)
        let context = UIGraphicsGetCurrentContext()
        
        context!.saveGState()
        path.addClip()
        color.setStroke()
        path.lineWidth = lineWidth
        path.stroke()
        
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage
    }
    
    
    func removeTabbBarItemsText() {
        if let items = tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources  that can be recreated.
    }
    
}
