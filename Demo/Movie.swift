//
//  Movie.swift
//  Demo
//
//  Created by Toma on 7/14/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import Foundation

class Movie {
    var voteCount: Int?
    var posterPath: String?
    var overview: String?
    var title: String?
    var id: Int?
    
    init(voteCount: Int?, posterPath: String?, overview: String?, title: String?, id: Int?) {
        self.voteCount = voteCount
        self.posterPath = posterPath
        self.overview = overview
        self.title = title
        self.id = id
    }
}
