//
//  LoginViewController.swift
//  Demo
//
//  Created by Toma on 7/5/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var noRegistrationButton: UIButton!
    
    private enum RedButtonState {
        case sendCode
        case signIn
    }
    
    private var redButtonState = RedButtonState.sendCode
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func hideView(view: UIView) {
        view.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func customizeSkipField() {
        let myString:NSString = "No, thanks. SKIP"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont(name: "Proxima Nova", size: 12.0)!])
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "ProximaNova-Semibold", size: 12.0)!, range: NSRange(location: 12, length: 4))
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: myString.length   ))
        noRegistrationButton.setAttributedTitle(myMutableString, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneNumberTextField.delegate = self
        passwordTextField.delegate = self
        
        hideKeyboardWhenTappedAround()
        
        phoneView.layer.cornerRadius = 5.0
        passwordView.layer.cornerRadius = 5.0
        hideView(view: passwordView)
        
        redButton.titleLabel?.text = "SEND CODE VIA SMS"
        
        customizeSkipField()
        
        DBManager.sharedInstance.dummyFill()
    }
    
    private func validPhoneNumber(number: String) -> Bool {
        if number.characters.count == 10 && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: number)) {
            return true
        }
        return false
    }
    
    private func validPhoneNumberWithAlert(number: String) -> Bool {
        if !validPhoneNumber(number: number) {
            let alert = UIAlertController(title: "Error", message: "The phone number must be 10 digits long", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    private func darken(view: UIView) {
        let blackOverlay = UIView(frame: view.frame)
        blackOverlay.layer.backgroundColor = UIColor.black.cgColor
        blackOverlay.layer.opacity = 0.3
        view.addSubview(blackOverlay)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "register":
                if let vc = segue.destination as? PasswordViewController {
                    if let phoneNumber = phoneNumberTextField.text {
                        vc.phoneNumber = phoneNumber
                    }
                }
            default:
                break
            }
        }
    }
    
    func validateFields() -> Bool {
        if passwordTextField.text == "" {
            let alert = UIAlertController(title: "Error", message: "You must enter your password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert,
                         animated: true, completion: nil)
            return false
        }
        if let phoneNumber = phoneNumberTextField.text, let password = passwordTextField.text, DBManager.sharedInstance.goodCredentials(forPhoneNumber: phoneNumber, password: password) {
            performSegue(withIdentifier: "login_to_main", sender: nil)
            return true
        }
        
        let alert = UIAlertController(title: "Error", message: "The password does not match the phone number you have entered", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        return false
    }
    
    @IBAction func redButtonPressed(_ sender: UIButton) {
        if redButtonState == .sendCode {
            if let number = phoneNumberTextField.text {
                if validPhoneNumberWithAlert(number: number) {
                    DBManager.sharedInstance.fetchUsers { (result, error) in
                        if error != nil {
                            print("Error: " + String(describing: error))
                        }
                        else {
                            var ok = false
                            if let result = result {
                                for user in result {
                                    if user.phoneNumber == number {
                                        //The user exists in the database
                                        print("The user exists")
                                        ok = true
                                        
                                        let view = passwordView
                                        UIView.animate(withDuration: 1) {
                                            view?.isHidden = false
                                        }
                                        
                                        redButtonState = .signIn
                                        redButton.setTitle("SIGN IN", for: .normal)
                                        
                                        phoneNumberTextField.isUserInteractionEnabled = false
                                        darken(view: phoneView)
                                        
                                        break
                                    }
                                }
                            }
                            if ok == false {
                                self.performSegue(withIdentifier: "register", sender: nil)
                            }
                        }
                        
                    }
                }
            }
        }
        else if redButtonState == .signIn {
            //Sign in (the phone number exists)
            if validateFields() {
                DBManager.sharedInstance.setCurrentUser(phoneNumber: phoneNumberTextField.text!)
                if let phoneNumber = phoneNumberTextField.text { 
                    CurrentUser.sharedInstance.set(user: DBManager.sharedInstance.fetchUser(withPhoneNumber: phoneNumber))
                }
            }
            
        }
    }
    
    @IBAction func handlePhoneViewTap(_ sender: UITapGestureRecognizer) {
        phoneNumberTextField.becomeFirstResponder()
    }
    
    @IBAction func handlePasswordViewTap(_ sender: UITapGestureRecognizer) {
        if passwordTextField.isFirstResponder == false {
            passwordTextField.becomeFirstResponder()
        }
    }
  }
