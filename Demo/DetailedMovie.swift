//
//  DetailedMovie.swift
//  Demo
//
//  Created by Toma Radu-Petrescu on 16/07/2017.
//  Copyright © 2017 Toma. All rights reserved.
//

import Foundation

class DetailedMovie {
    var id: Int?
    var releaseDate: String?
    var title: String?
    var overview: String?
    var posterPath: String?
    
    init(id: Int?, releaseDate: String?, title: String?, overview: String?, posterPath: String?) {
        self.id = id
        self.releaseDate = releaseDate
        self.title = title
        self.overview = overview
        self.posterPath = posterPath
    }
}
