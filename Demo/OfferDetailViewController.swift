//
//  OfferDetailViewController.swift
//  Demo
//
//  Created by Toma on 7/13/17.
//  Copyright © 2017 Toma. All rights reserved.
//

import UIKit
import MapKit

class OfferDetailViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var headerViewtTitle: UILabel!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    var id: Int?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func addHeaderViewShadow() {
        headerView.layer.shadowColor = UIColor.gray.cgColor;
        headerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        headerView.layer.shadowOpacity = 0.5;
        headerView.layer.shadowRadius = 3;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let myBackButton:UIButton = UIButton.init(frame: CGRect(x: -40, y: 0, width: 50, height: 30)
        )
        myBackButton.addTarget(self, action: #selector(OfferDetailViewController.popToRoot(sender:)), for: .touchUpInside)
        myBackButton.setTitle("Back", for: .normal)
        myBackButton.setImage(UIImage(named: "arrow_left"), for: .normal)
        myBackButton.setAttributedTitle(NSAttributedString(string: "BACK", attributes: [NSFontAttributeName: UIFont(name: "ProximaNova-Regular", size: 9)!, NSKernAttributeName: 1.2, NSForegroundColorAttributeName: UIColor.white]), for: .normal)
        
        myBackButton.contentHorizontalAlignment = .left
        myBackButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        myBackButton.titleEdgeInsets.left = 10
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
        
        
    }
    
    func populate(result: DetailedMovie) {
        DispatchQueue.main.async {
            if let posterPath = result.posterPath {
                
                self.indicatorView.isHidden = false
                
                self.indicatorView.startAnimating()
                
                let urlComponents = URLComponents(string: Constants.BASE_API_IMAGE_LINK + "/" + posterPath)
                
                if let url = urlComponents?.url?.absoluteString {
                    NetworkManager.sharedInstance.getImage(fromURL: url, completionHandler: { (image, error) in
                        DispatchQueue.main.async {
                            
                            if error != nil {
                                print("Error", error!)
                                
                            }
                            else {
                                self.imageView.image = image
                            }
                            self.indicatorView.stopAnimating()
                            self.indicatorView.isHidden = true
                        }
                    })
                }
                
            }
            
            self.titleLabel.text = result.title
            self.overviewLabel.text = result.overview
            self.headerViewtTitle.text = result.releaseDate
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated:false)
        
        addHeaderViewShadow()
        
        self.indicatorView.isHidden = false
        
        if let id = id {
            NetworkManager.sharedInstance.apiRequest(forMovieId: id) { (result, error) in
                if error != nil {
                    print("Error", error!)
                }
                else if let result = result {
                    self.populate(result: result)
                }
            }
        }
    }
    
    
    func popToRoot(sender:UIBarButtonItem){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pinButtonPressed(_ sender: UIButton) {
        if let mapVC = tabBarController?.viewControllers?[2] as? MapsViewController {
            mapVC.location = CLLocationCoordinate2DMake(37.3092828, -122.061154)
            tabBarController?.selectedIndex = 2
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
